package Selenide;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;

public class AvitoSearchPage {
    private ElementsCollection titles = $$x("//h3");

    public List<String> getAllTitles() {
        List<String> titlesFromAvito = new ArrayList<>();
        titles.forEach(x -> titlesFromAvito.add(x.getText()));
        return titlesFromAvito;
    }
}

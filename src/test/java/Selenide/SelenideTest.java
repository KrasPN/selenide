package Selenide;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Locale;

import static com.codeborne.selenide.Selenide.*;

class SelenideTest {

    @Test
    public void avito() throws InterruptedException {
        open("https://www.avito.ru/");
        open("https://www.yandex.ru/");
        open("https://www.avito.ru/");
        AvitoSearchBar avitoSearchBar = new AvitoSearchBar();
        AvitoSearchPage avitoSearchPage = avitoSearchBar.search("Samsung");
        Thread.sleep(5000);
        executeJavaScript("window.stop();");
        List<String> titles = avitoSearchPage.getAllTitles();
        ElementsCollection prices = $$x("//span[@class= 'price-text-_YGDY text-text-LurtD text-size-s-BxGpL']");
        System.out.println("Общее количество ценников: " + prices.size());
        for (SelenideElement price :
                prices) {
            int value = Integer.parseInt(price.getText().replace('₽', ' ').replaceAll("\\s",""));
            System.out.println("Цена: " + value);
            Assertions.assertTrue(value < 200000);
        }
        closeWebDriver();
    }

}
package Selenide;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;

public class AvitoSearchBar {
    private SelenideElement searchField = $(By.xpath("//input[@type=\"text\"]"));

    public AvitoSearchPage search(String str) {
        searchField.sendKeys(str, Keys.ENTER);
        return new AvitoSearchPage();
    }

}
